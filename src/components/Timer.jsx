import React, {useState, useEffect} from 'react';

export default function Timer(){
    let [timer, setTimer] = useState(0);

    useEffect(() => {
        //Component Will Unmount
        const timerInterval = setInterval(() => {
            console.log(timer);
            setTimer(timer+=1);
        }, 1000);

        // componentWillUnmount
        return () => clearInterval(timerInterval);
    }, []);
    
    return (
        <div>
            <h3>{timer}</h3>
        </div>
    )
}