import React, {useState, useEffect} from 'react';
import Timer from './components/Timer';
import './App.css';

function App() {
  //estadoSaludo = useState('Bienvenidos al modulo de desarrollo web full stack');
  //saludo = estadoSaludo[0];
  //setSaludo = estadoSaludo[1];
  const [saludo, setSaludo] = useState('Bienvenidos al modulo de desarrollo web full stack');
  let [contador, setContador] = useState(0);
  const [user, setUser] = useState({
    name: 'oscar',
    email: 'oscar@academlo.com'
  });
  const [showTimer, setShowTimer] = useState(true);
  const [pokemons, setPokemons] = useState([]);
  const [detail, setDetail] = useState([]);

  const cambiarSaludo = () => {
    setSaludo('Hello world!')
  }

  useEffect(() => {
    //Component Did update
    console.log(user);
  });

  const getDetail = async(arregloPokemones) => {
    let tempArrayDetail = [];
    return new Promise(async (resolve, reject) => {
      await arregloPokemones.forEach( async(pokemon) => {
        //Iteramos cada pokemon del arreglo y hacemos una peticion para sacar el detalle
        let responseDetail = await fetch(pokemon.url);
        let dataDetail = await responseDetail.json();
        tempArrayDetail.push(dataDetail);
      });
      resolve(tempArrayDetail);
    })
  }


  useEffect(async () => {
    //Component Did Mount
    let response = await fetch('https://pokeapi.co/api/v2/pokemon?limit=10');
    let data = await response.json();
    let results = data.results;
   
    let detallePokemones = await getDetail(results);
    
    setDetail(detallePokemones);
    setPokemons(results);

    // this.setState({pokemons: pokemons}, () => {
      
    // });

  }, []);



  return (
    <div className="App">
      <h3>{saludo}</h3>
      <button onClick={cambiarSaludo}>Cambiar saludo</button>
      <h2>{contador}</h2>
      <button onClick={() => setContador(contador+=1)}>Aumentar</button>
      {
        showTimer && <Timer />
      }
      <button onClick={() => setShowTimer(!showTimer)}>Ocultar timer</button>
      {pokemons.map( (pokemon, index) => <h5 key={index}>{pokemon.name}</h5>)}
    </div>
  );
}

export default App;
